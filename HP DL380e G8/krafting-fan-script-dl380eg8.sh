#!/usr/bin/env bash
#
# This file is part of the ilo4_unlock (https://github.com/kendallgoto/ilo4_unlock/).
# Copyright (c) 2022 Kendall Goto.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# NOTE: This is just what I use on my system! I DON'T suggest these fan curves
# since they are aggressive and unsafe. I play loose since I'm lazy. Here's some setup notes:
# I write iLO directly to this system, running Proxmox
# I have this script loaded at boot with crontab @reboot

# This setup is tuned for my DL380e Gen8 LFF server. It needs a SSH key setup.

IP=CHANGE_ME
SSH_USER="Administrator"

# Old usecase, without ssh key:
# PASSWD="/home/krafting/Desktop/passfile"
# Options to use, I've put them in the .ssh/config file:
# OPTIONS="-oHostKeyAlgorithms=+ssh-rsa -oKexAlgorithms=+diffie-hellman-group1-sha1 -oPubkeyAcceptedAlgorithms=+ssh-rsa"

SCREEN_NAME=ilo4fan

while [ true ]
do
	echo -e "Checking if ${IP} is up."
	ping -q -c 1 ${IP} &>/dev/null
	if [ $? -ne 0 ]; then
		echo "iLO is not responding. Reattempting in 30 seconds.";
	else
		echo "iLO is responding, silencing now..."
		break
	fi
	sleep 30
done

if screen -list | grep -q $SCREEN_NAME; then
    # run bash script
    echo -e "Screen exists.. Killing it.."
    screen -X -S $SCREEN_NAME quit
    sleep 0.1
fi

IP=${IP} screen -dmS $SCREEN_NAME

echo -e "Establishing SSH session inside screen."
screen -S $SCREEN_NAME -X stuff "ssh -t  ${SSH_USER}@${IP} -o LocalCommand='fan info' -v"`echo -ne '\015'`
sleep 0.1


echo -e "Sleeping for 5 seconds (Waiting for SSH connection to be established)."

sleep 5

echo -e "Resuming fan commands."

# `echo -ne '\015'` emulates pressing the Enter key.

screen -S $SCREEN_NAME -X stuff 'fan info'`echo -ne '\015'`
sleep 0.1
sleep 0.1

##############################
# Disabling Unwanted Sensors #
##############################
# Note: Please check that that temps are not going to cause any issue before uncommenting lines here.
##############################
echo -e "Disabling some sensors all together"
### System Board
# screen -S $SCREEN_NAME -X stuff 'fan t 52 off'`echo -ne '\015'`
# sleep 0.1
# screen -S $SCREEN_NAME -X stuff 'fan t 53 off'`echo -ne '\015'`
# sleep 0.1

### PCI Zone 1
# screen -S $SCREEN_NAME -X stuff 'fan t 34 off'`echo -ne '\015'`
# sleep 0.1
### PCI Zone 2
# screen -S $SCREEN_NAME -X stuff 'fan t 35 off'`echo -ne '\015'`
# sleep 0.1

### HDD Front Cage
screen -S $SCREEN_NAME -X stuff 'fan t 11 off'`echo -ne '\015'`
sleep 0.1

### PCI Zone 3
# screen -S $SCREEN_NAME -X stuff 'fan t 36 off'`echo -ne '\015'`
# sleep 0.1

### PCI Zone 4
# screen -S $SCREEN_NAME -X stuff 'fan t 37 off'`echo -ne '\015'`
# sleep 0.1
echo -e "Done."


# Changing low speed
# 20 => 2000
### System Board (if not disabled)
echo -e "Fixing lowest valueof PWM for most pid"
screen -S $SCREEN_NAME -X stuff 'fan pid 52 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 53 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 1 Zone 
screen -S $SCREEN_NAME -X stuff 'fan pid 34 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 35 lo 1600'`echo -ne '\015'`
sleep 0.1

# Set lower low (base=24, now 16)
screen -S $SCREEN_NAME -X stuff 'fan pid 01 lo 1600'`echo -ne '\015'`
sleep 0.1

### CPU 1
screen -S $SCREEN_NAME -X stuff 'fan pid 02 lo 1600'`echo -ne '\015'`
sleep 0.1
### CPU 2
screen -S $SCREEN_NAME -X stuff 'fan pid 03 lo 1600'`echo -ne '\015'`
sleep 0.1
### Memory
screen -S $SCREEN_NAME -X stuff 'fan pid 04 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 05 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 06 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 07 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 08 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 09 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 10 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 11 lo 1600'`echo -ne '\015'`
sleep 0.1
### HDD Front Cage
screen -S $SCREEN_NAME -X stuff 'fan pid 12 lo 1600'`echo -ne '\015'`
sleep 0.1
### Chipset
screen -S $SCREEN_NAME -X stuff 'fan pid 13 lo 1600'`echo -ne '\015'`
sleep 0.1
### Chipset Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 14 lo 1600'`echo -ne '\015'`
sleep 0.1
### P/S 1 & P/S 2
screen -S $SCREEN_NAME -X stuff 'fan pid 15 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 16 lo 1600'`echo -ne '\015'`
sleep 0.1
### P/S 1 Zone & P/S 2 Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 17 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 18 lo 1600'`echo -ne '\015'`
sleep 0.1
### VR P1 & VR P2
screen -S $SCREEN_NAME -X stuff 'fan pid 19 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 20 lo 1600'`echo -ne '\015'`
sleep 0.1
### VR P1 Mem & VR P2 Mem
screen -S $SCREEN_NAME -X stuff 'fan pid 21 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 22 lo 1600'`echo -ne '\015'`
sleep 0.1
### SuperCap Max
screen -S $SCREEN_NAME -X stuff 'fan pid 23 lo 1600'`echo -ne '\015'`
sleep 0.1
### iLO Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 26 lo 1600'`echo -ne '\015'`
sleep 0.1
### LOM Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 28 lo 1600'`echo -ne '\015'`
sleep 0.1

## PCI
### PCI 2
screen -S $SCREEN_NAME -X stuff 'fan pid 30 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 3
screen -S $SCREEN_NAME -X stuff 'fan pid 31 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 4
screen -S $SCREEN_NAME -X stuff 'fan pid 32 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 33 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 34 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 1 Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 35 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 2 Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 36 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 3 Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 37 lo 1600'`echo -ne '\015'`
sleep 0.1
### PCI 4 Zone
screen -S $SCREEN_NAME -X stuff 'fan pid 38 lo 1600'`echo -ne '\015'`
sleep 0.1
### System Board
screen -S $SCREEN_NAME -X stuff 'fan pid 41 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 42 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 43 lo 1600'`echo -ne '\015'`
sleep 0.1
### Sys Exhaust
screen -S $SCREEN_NAME -X stuff 'fan pid 48 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 49 lo 1600'`echo -ne '\015'`
sleep 0.1

### Other Board
screen -S $SCREEN_NAME -X stuff 'fan pid 50 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 54 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 55 lo 1600'`echo -ne '\015'`
sleep 0.1

# Others
screen -S $SCREEN_NAME -X stuff 'fan pid 29 lo 1600'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan pid 31 lo 1600'`echo -ne '\015'`
sleep 0.1
echo -e "Done."


#fix minimums
# Disable Front Ambient Sensor when it's hot:
#screen -S $SCREEN_NAME -X stuff 'fan t 00 set 5'`echo -ne '\015'`
#sleep 0.1

# Set fan minimum
# 15 : 5%
# 25 : 9%
# 30 : 11%
# 40 : 15%
# 50 : 20%
echo -e "Fixing Minimum value of fans"
screen -S $SCREEN_NAME -X stuff 'fan p 0 min 25'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan p 1 min 25'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan p 2 min 25'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan p 3 min 25'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan p 4 min 25'`echo -ne '\015'`
sleep 0.1
screen -S $SCREEN_NAME -X stuff 'fan p 5 min 25'`echo -ne '\015'`
sleep 0.1
echo -e "Done."


echo -e "Script finished. You can use the screen $SCREEN_NAME to send commands to iLO with : screen -r $SCREEN_NAME"
