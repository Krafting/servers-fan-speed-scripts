#!/bin/bash
# Enable manual fan control
ipmitool raw 0x30 0x30 0x01 0x00

if [ ! -f /lib/systemd/system/fanspeed-r715.service ]; then
echo """[Unit]
Description=Fan control for Dell R715 by krafting

[Service]
WorkingDirectory=/var/
ExecStart=bash /var/fanspeed.sh
Restart=always
RestartSec=5s

[Install]
WantedBy=multi-user.target
""" > /lib/systemd/system/fanspeed-r715.service

# On reload les daemon systemd si on veux le lancer après
sudo systemctl daemon-reload
fi

while [ true ]
do
    tempcpu1=$(sensors -j  | python3 -c "import sys, json; print(json.load(sys.stdin)['k10temp-pci-00d3']['temp1']['temp1_input'])")
    tempcpu2=$(sensors -j  | python3 -c "import sys, json; print(json.load(sys.stdin)['k10temp-pci-00db']['temp1']['temp1_input'])")

    if [ 1 -eq "$(echo "${tempcpu1} < 10" | bc)" ]
    then 
        fanspeed=5
    elif [[ 1 -eq "$(echo "${tempcpu1} > 9" | bc)" && 1 -eq "$(echo "${tempcpu1} < 15" | bc)" ]]
    then 
        fanspeed=10
    elif [[ 1 -eq "$(echo "${tempcpu1} > 14" | bc)" && 1 -eq "$(echo "${tempcpu1} < 16" | bc)" ]]
    then 
        fanspeed=11
    elif [[ 1 -eq "$(echo "${tempcpu1} > 15" | bc)" && 1 -eq "$(echo "${tempcpu1} < 17" | bc)" ]]
    then 
        fanspeed=12
    elif [[ 1 -eq "$(echo "${tempcpu1} > 16" | bc)" && 1 -eq "$(echo "${tempcpu1} < 18" | bc)" ]]
    then 
        fanspeed=13
    elif [[ 1 -eq "$(echo "${tempcpu1} > 17" | bc)" && 1 -eq "$(echo "${tempcpu1} < 19" | bc)" ]]
    then 
        fanspeed=14
    elif [[ 1 -eq "$(echo "${tempcpu1} > 18" | bc)" && 1 -eq "$(echo "${tempcpu1} < 20" | bc)" ]]
    then 
        fanspeed=15
    elif [[ 1 -eq "$(echo "${tempcpu1} > 19" | bc)" && 1 -eq "$(echo "${tempcpu1} < 21" | bc)" ]]
    then 
        fanspeed=16
    elif [[ 1 -eq "$(echo "${tempcpu1} > 20" | bc)" && 1 -eq "$(echo "${tempcpu1} < 22" | bc)" ]]
    then 
        fanspeed=17
    elif [[ 1 -eq "$(echo "${tempcpu1} > 21" | bc)" && 1 -eq "$(echo "${tempcpu1} < 23" | bc)" ]]
    then 
        fanspeed=18
    elif [[ 1 -eq "$(echo "${tempcpu1} > 22" | bc)" && 1 -eq "$(echo "${tempcpu1} < 24" | bc)" ]]
    then 
        fanspeed=19
    elif [[ 1 -eq "$(echo "${tempcpu1} > 23" | bc)" && 1 -eq "$(echo "${tempcpu1} < 25" | bc)" ]]
    then 
        fanspeed=20
    elif [[ 1 -eq "$(echo "${tempcpu1} > 24" | bc)" && 1 -eq "$(echo "${tempcpu1} < 26" | bc)" ]]
    then 
        fanspeed=21
    elif [[ 1 -eq "$(echo "${tempcpu1} > 25" | bc)" && 1 -eq "$(echo "${tempcpu1} < 30" | bc)" ]]
    then 
        fanspeed=22
    elif [[ 1 -eq "$(echo "${tempcpu1} > 29" | bc)" && 1 -eq "$(echo "${tempcpu1} < 35" | bc)" ]]
    then 
        fanspeed=25
    elif [[ 1 -eq "$(echo "${tempcpu1} > 34" | bc)" && 1 -eq "$(echo "${tempcpu1} < 40" | bc)" ]]
    then 
        fanspeed=30
    elif [[ 1 -eq "$(echo "${tempcpu1} > 39" | bc)" && 1 -eq "$(echo "${tempcpu1} < 45" | bc)" ]]
    then 
        fanspeed=35
    elif [[ 1 -eq "$(echo "${tempcpu1} > 44" | bc)" && 1 -eq "$(echo "${tempcpu1} < 50" | bc)" ]]
    then 
        fanspeed=40
    elif [[ 1 -eq "$(echo "${tempcpu1} > 49" | bc)" && 1 -eq "$(echo "${tempcpu1} < 60" | bc)" ]]
    then 
        fanspeed=50
    elif [[ 1 -eq "$(echo "${tempcpu1} > 59" | bc)" && 1 -eq "$(echo "${tempcpu1} < 65" | bc)" ]]
    then 
        fanspeed=75
    else
        fanspeed=100
    fi
    fanspeedhex=$(printf '%x\n' $fanspeed)
    ipmitool raw 0x30 0x30 0x02 0xff 0x$fanspeedhex
    echo "Temperature : $tempcpu1 Fan Speed: $fanspeed%"
    sleep 5
done